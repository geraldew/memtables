# MemTables

This is a minimal implementation of an idea. Put here on its own as example, but likely to be incorporated into other works.

The idea is to do memory holding of selected tables, to load data from each table on first request then serve it from memory.
For simplicity the model will be to presume an integer value for each row key

## Software License

This is Free Open Source Software (FOSS) using the GPL3 license.

As a note to those who may find that concerning, let me be clear:
- the author has followed the evolution of FOSS since its birth and is well versed in the concepts;
- Yes, I do know that GPL is not the norm for Python applications;
- Copyright - and thus the license - applies to my specific code, so a fork of this source code has to also respect and apply the GPL;
- But FOSS means you are free to study the code and use what you learn to write your own software, where you can thereby choose your license. No need to ask, please go ahead and do that.
