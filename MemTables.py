#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# MemTables

# The idea is to do memory holding of selected tables
# to load data from each table on first request then serve it from memory
# For simplicity the model will be to presume an integer value for each row key
# Struturally it will be a set of nested dictionaries:
# - one for the tables - keyed by table names - then inside this:
#   - one for the columns, keyed by column names, holding their datatypes
#   - one for the rows - keyed by row ID - then in each:
#     - one per row - keyed by column name - with the data

# --------------------------------------------------
# Copyright (C) 2022  Gerald Waters - Melbourne, Australia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# --------------------------------------------------
# Imports - Standard
# --------------------------------------------------

import sqlite3
from sqlite3 import Error

# Dictionary fixed keys - as the functions will be used, the values don't matter as long as they're unique
def MmTbl_Key_Loaded() :
	return "L" # boolean
def MmTbl_Key_Columns() :
	return "C" # dictionary
def MmTbl_Key_KeyCol() :
	return "K" # string
def MmTbl_Key_Rows() :
	return "R" # dictionary

# --------------------------------------------------------
# Specific values for the example run with functions providing immutable values
# --------------------------------------------------

def TestVal_Database_Filename():
	return "/home/ger/tkinter_dbase.db"
def TestVal_Tablename():
	return "POST_TYPE"
def TestVal_IdColumnName():
	return "LEDGER_TYPE_NO"
def TestVal_ValColumnNameA():
	return "LEDGER_TYPE_DESCRIPTION"
def TestVal_ValColumnNameB():
	return "LEDGER_NAME"
def TestVal_IdExampleA():
	return 4000
def TestVal_IdExampleB():
	return 3000

# --------------------------------------------------
# SQLite Wrappers
# --------------------------------------------------

def Sqlite_Connection_Close( p_conn):
	r_didok = False
	try:
		if p_conn:
			p_conn.close()
			r_didok = True
	except Error as i_e:
		print("SQLite Error: " + str( i_e) )
	return r_didok

def Sqlite_Connection_Create_ToFile( p_db_file):
	r_conn = None
	r_didok = False
	try:
		r_conn = sqlite3.connect( p_db_file)
		r_conn.row_factory = sqlite3.Row
		r_didok = True
	except Error as i_e:
		print("SQLite Error: " + str( i_e) )
	return r_didok, r_conn

def Sqlite_Sql_FetchRecords_WithColumnNames( p_conn, p_str_sql):
	r_didok = False
	try:
		c = p_conn.cursor()
		c.execute( p_str_sql )
		r_rows = c.fetchall()
		r_didok = True
	except Error as e:
		print("SQLite Error: " + str(e) )
		r_rows = []
		r_names = []
	if r_didok :
		r_names = list( map( lambda x: x[0], c.description) )
	return r_didok, r_rows, r_names

# --------------

def SqLite_Filename_GetTable_Rows( p_fn, p_tblnm ):
	r_didok = False
	r_rows = []
	r_names = []
	r_didok, i_conn = Sqlite_Connection_Create_ToFile( p_fn)
	if r_didok :
		i_str_sql = "SELECT * FROM " + p_tblnm + " ;"
		r_didok, r_rows, r_names = Sqlite_Sql_FetchRecords_WithColumnNames( i_conn, i_str_sql)
		Sqlite_Connection_Close( i_conn)
	return r_didok, r_rows, r_names

def SqLite_Filename_GetTable_Rows_Where_ForeignKey_IsValue( p_fn, p_tblnm, p_fkc, p_kval ):
	r_didok = False
	r_rows = []
	r_names = []
	r_didok, i_conn = Sqlite_Connection_Create_ToFile( p_fn)
	if r_didok :
		i_str_sql = "SELECT * FROM " + p_tblnm + " WHERE " + p_fkc + " = " + str(p_kval) + " ;"
		r_didok, r_rows, r_names = Sqlite_Sql_FetchRecords_WithColumnNames( i_conn, i_str_sql)
		Sqlite_Connection_Close( i_conn)
	return r_didok, r_rows, r_names

# --------------------------------------------------------
# MemTables
# --------------------------------------------------------

def MmTbl_Table_Load_SQLite( MemTable_Dct, p_tblnm ):
	# print("MmTbl_Table_Load_SQLite")
	didok = True # unless/until something goes wrong
	KyColNm = MemTable_Dct[ p_tblnm ][ MmTbl_Key_KeyCol() ]
	OneTable_Dct = {}
	OneTable_Dct[ MmTbl_Key_KeyCol() ] = KyColNm
	# do the SQLite fetch
	p_fn = ""
	didok, rows, names = SqLite_Filename_GetTable_Rows( TestVal_Database_Filename(), p_tblnm )
	# now work out how to fit that into the MemTable structures
	# have a pointer to the table's columns dictionary object
	ClmList_Dct = {}
	for fld in names :
		ClmList_Dct[ fld ] = fld # a bit of a bodge as the original stored the .Type here - but then didn't use it anyway
	OneTable_Dct[ MmTbl_Key_Columns() ] = ClmList_Dct
	# have a pointer to the table's rows dictionary object
	TblRows_Dct = {}
	for r in rows :
		# build a fresh row dictionary
		OneRow_Dct = {}
		for k in r.keys():
			v = r[k]
			if k == KyColNm :
				rowkey = v
			OneRow_Dct[ k ] = v
		TblRows_Dct[ rowkey ] = OneRow_Dct
	OneTable_Dct[ MmTbl_Key_Rows() ] = TblRows_Dct
	MemTable_Dct[ p_tblnm ] = OneTable_Dct
	if didok :
		OneTable_Dct[ MmTbl_Key_Loaded() ] = True
	return didok

def MmTbl_Table_Load_Data( MemTable_Dct, tblnam) :
	# This is a redirection layer in case we change the implementation layer - initially done as SQLite
	if tblnam in MemTable_Dct :
		did_data_ok = MmTbl_Table_Load_SQLite( MemTable_Dct, tblnam )
		didok = did_data_ok
	else :
		didok = False
	return didok

def MmTbl_Table_Is_Loaded( MemTable_Dct, tblname ) :
	b = False
	if tblname in MemTable_Dct :
		OneTable_Dct = MemTable_Dct[ tblname ]
		if MmTbl_Key_Loaded() in OneTable_Dct :
			b = OneTable_Dct[ MmTbl_Key_Loaded() ]
	return b

def MmTbl_Table_Is_Known( MemTable_Dct, tblname )  :
	return tblname in MemTable_Dct

def MmTbl_Table_Setup( MemTable_Dct, tblname, KyColNm ) :
	# Prepare the new table item
	OneTabl_Dct = {}
	OneTabl_Dct[ MmTbl_Key_Loaded() ] = False
	# an empty dictionary for the Column details
	OneTabl_Dct[ MmTbl_Key_Columns() ] = {}
	# set the key column name
	OneTabl_Dct[ MmTbl_Key_KeyCol() ] = KyColNm
	# set an empty dictionary for the Rows of data
	OneTabl_Dct[ MmTbl_Key_Rows() ] = {}
	# Clear any existing entry for this table name
	if MmTbl_Table_Is_Known( MemTable_Dct, tblname) :
		MemTable_Dct[ tblname ] = None
	# Now add the new Table item to the dictionary of held tables
	MemTable_Dct[ tblname ] = OneTabl_Dct

def MmTbl_Table_Refresh( MemTable_Dct, tblnam ) :
	# this just ensures we don't accumulate rows by clearing out the current holding
	if tblnam in MemTable_Dct :
		# get the key column name
		KyColNm = MemTable_Dct[ tblnam ][ MmTbl_Key_KeyCol() ]
		# re-setup the table
		MmTbl_Table_Setup( MemTable_Dct, tblnam, KyColNm )
		# re call the load
		didok = MmTbl_Table_Load_Data( MemTable_Dct, tblnam )
	else :
		didok = False
	return didok

def MmTbl_Row_Get( MemTable_Dct, tblname, Key ) :
	if MmTbl_Table_Is_Known( MemTable_Dct, tblname ) :
		if MmTbl_Table_Is_Loaded( MemTable_Dct, tblname ) :
			is_ok = True
		else :
			is_ok = MmTbl_Table_Load_Data( MemTable_Dct, tblname )
		if is_ok and tblname in MemTable_Dct :
			OneTable_Dct = MemTable_Dct[ tblname ]
			TableRows_Dct = OneTable_Dct[ MmTbl_Key_Rows() ]
			print( "Seeking Key:" + str(Key) )
			datarow_dct = TableRows_Dct[ Key ]
			didok = True
		else :
			didok = False
	else :
		didok = False
	return datarow_dct, didok

def MmTbl_ColumnValue_Get( MemTable_Dct, tblname , Key , ColName ) :
	if MmTbl_Table_Is_Known( MemTable_Dct, tblname) :
		one_row_dct, didok = MmTbl_Row_Get( MemTable_Dct, tblname, Key )
		if didok :
			if ColName in one_row_dct :
				dataval = one_row_dct[ ColName ]
				didok = True
			else :
				didok = False
		else :
		  didok = False
	else :
		didok = False
	return dataval, didok

# --------------------------------------------------------
# Example usage and test call
# --------------------------------------------------------

def MemTable_Test() :
	print("MemTable_Test")
	print("Setting empty MemTable dictionary")
	MemTable_Dct = {}
	print("Setting Table: " + TestVal_Tablename() + " with key column " + TestVal_IdColumnName() )
	MmTbl_Table_Setup( MemTable_Dct, TestVal_Tablename(), TestVal_IdColumnName() )
	print("Fetching for id=" + str(TestVal_IdExampleA()) + " value in column: " + TestVal_ValColumnNameA() )
	dataval, didok = MmTbl_ColumnValue_Get( MemTable_Dct, \
		TestVal_Tablename(), TestVal_IdExampleA(), TestVal_ValColumnNameA() )
	if didok :
		print( "Got value: " + dataval )
	else :
		print( "Getting example failed for " + str(TestVal_IdExampleA() ) )
	print("Refreshing table:" + TestVal_Tablename() )
	didok = MmTbl_Table_Refresh( MemTable_Dct, TestVal_Tablename() )
	if didok :
		print("Fetching for id=" + str(TestVal_IdExampleB()) + " value in column: " + TestVal_ValColumnNameB() )
		dataval, didok = MmTbl_ColumnValue_Get( MemTable_Dct, \
			TestVal_Tablename(), TestVal_IdExampleB(), TestVal_ValColumnNameB() )
		if didok :
			print( "Got value: " + dataval )
		else :
			print( "Getting example failed for " + str(TestVal_IdExampleB() ) )
	else :
		print( "Refresh failed!" )

# test call
MemTable_Test()
